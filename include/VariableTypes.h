// VariableTypes.hpp
// indef defines a header guard, which prevents the contents of the header from being defined more than once.
#ifndef VARIABLE_TYPES_HPP
// define the header guard for VariableTypes.hpp file to prevent multiple inclusion
#define VARIABLE_TYPES_HPP

// Function declaration
void showVariableTypes();

#endif // VARIABLE_TYPES_HPP
