// EqualityTest.cpp
#include "../include/EqualityTest.h"
#include <iostream>
#include <string>

void showEqualityTests() {
    std::cout << "Demonstrating equality tests in C++:" << std::endl;

    // Integer equality
    int a = 5, b = 5, c = 3;
    std::cout << "\tInteger equality: " << a << " == " << b << " ? " << (a == b) << std::endl;
    std::cout << "\tInteger inequality: " << a << " == " << c << " ? " << (a == c) << std::endl;

    // Floating-point equality (demonstrating potential precision issues)
    double d = 0.3, e = 0.1 + 0.2;
    std::cout << "\tFloating-point equality: " << d << " == " << e << " ? " << (d == e) << std::endl;
    std::cout << "\t(Note: Floating-point comparison might not be accurate due to precision issues.)" << std::endl;

    // String equality
    std::string first = "hello", second = "hello", third = "world";
    std::cout << "\tString equality: " << first << " == " << second << " ? " << (first == second) << std::endl;
    std::cout << "\tString inequality: " << first << " == " << third << " ? " << (first == third) << std::endl;
}
