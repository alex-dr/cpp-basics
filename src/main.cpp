#include <iostream>
#include "../include/VariableTypes.h"
#include "../include/MathOperations.h"
#include "../include/StringOperations.h"
#include "../include/ArrayOperations.h"
#include "../include/EqualityTest.h"
#include "../include/ExceptionHandling.h"
#include "../include/PointersExample.h"



int main() {
    std::cout << "Hello, World!" << std::endl;
    showVariableTypes();
    performAndShowOperations();
    showStringOperations();
    showArrayOperations();
    showEqualityTests();
    showExceptionHandling();
    showPointerExamples();




    return 0;
}
