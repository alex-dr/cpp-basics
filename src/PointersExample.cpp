// PointersExample.cpp
#include "../include/PointersExample.h"
#include <iostream>

// Function to demonstrate function pointer
void sampleFunction(int a) {
    std::cout << "\tInside sampleFunction with value " << a << std::endl;
}

void showPointerExamples() {
    std::cout << "Demonstrating pointer operations in C++:" << std::endl;

    // Basic pointer declaration and initialization
    int x = 10;
    int* ptr = &x;
    std::cout << "\tValue of x: " << x << ", Address of x: " << ptr << ", Value pointed by ptr: " << *ptr << std::endl;

    // Pointer arithmetic
    int arr[] = {10, 20, 30};
    int* arrPtr = arr;
    std::cout << "\tArray pointer points to: " << *arrPtr << std::endl;
    arrPtr++; // Pointing to next element
    std::cout << "\tArray pointer now points to: " << *arrPtr << std::endl;

    // Pointer to pointer
    int** ptrToPtr = &ptr;
    std::cout << "\tValue pointed by ptrToPtr: " << **ptrToPtr << std::endl;

    // Array of pointers
    int* arrOfPtrs[3];
    for (int i = 0; i < 3; ++i) {
        arrOfPtrs[i] = &arr[i];
    }
    std::cout << "\tValues via array of pointers: ";
    for (int i = 0; i < 3; ++i) {
        std::cout << *arrOfPtrs[i] << " ";
    }
    std::cout << std::endl;

    // Function pointer
    void (*funcPtr)(int) = sampleFunction;
    std::cout << "\tCalling sampleFunction via function pointer with 5: ";
    (*funcPtr)(5);
}
