// MathOperations.cpp
#include "../include/MathOperations.h"
#include <iostream>

// Define individual operations
int add(int a, int b) {
    return a + b;
}

int subtract(int a, int b) {
    return a - b;
}

int multiply(int a, int b) {
    return a * b;
}

double divide(int a, int b) {
    if (b == 0) {
        throw "Division by zero!";
    }
    return static_cast<double>(a) / b;
}

int modulus(int a, int b) {
    if (b == 0) {
        throw "Modulus by zero!";
    }
    return a % b;
}

// Perform operations and output the results to the console
void performAndShowOperations() {
    std::cout << "The main mathematical operations in C++ are:" << std::endl;

    int a = 1, b = 1;
    std::cout << "\tAddition:\t\t" << a << " + " << b << " = " << add(a, b) << std::endl;

    a = 3; b = 1;
    std::cout << "\tSubtraction:\t\t" << a << " - " << b << " = " << subtract(a, b) << std::endl;

    a = 2; b = 3;
    std::cout << "\tMultiplication:\t" << a << " * " << b << " = " << multiply(a, b) << std::endl;

    a = 6; b = 2;
    try {
        std::cout << "\tDivision:\t\t" << a << " / " << b << " = " << divide(a, b) << std::endl;
    } catch (const char* msg) {
        std::cerr << "\tError:\t\t" << msg << std::endl;
    }

    a = 10; b = 3;
    try {
        std::cout << "\tModulus:\t\t" << a << " % " << b << " = " << modulus(a, b) << std::endl;
    } catch (const char* msg) {
        std::cerr << "\tError:\t\t" << msg << std::endl;
    }

    // Add more operations as necessary
}
