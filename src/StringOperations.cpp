// StringOperations.cpp
#include "../include/StringOperations.h"
#include <iostream>
#include <string>

void showStringOperations() {
    std::cout << "The main string operations in C++ are:" << std::endl;

    // Concatenation
    std::string first = "Hello";
    std::string second = "World";
    std::string concatenated = first + " " + second;
    std::cout << "\tConcatenation:\t\t" << first << " + " << second << " = " << concatenated << std::endl;

    // Substring
    std::string substring = concatenated.substr(6, 5); // "World"
    std::cout << "\tSubstring:\t\t" << "substring of '" << concatenated << "' from index 6 for 5 characters = " << substring << std::endl;

    // Find
    size_t found = concatenated.find("World");
    std::cout << "\tFind:\t\t\t" << "'World' found in '" << concatenated << "' at index " << found << std::endl;

    // Replace
    std::string replaced = concatenated;
    replaced.replace(replaced.find("World"), 5, "C++");
    std::cout << "\tReplace:\t\t" << "replace 'World' with 'C++' in '" << concatenated << "' = " << replaced << std::endl;

    // Size
    std::cout << "\tSize:\t\t\t" << "'" << concatenated << "' has " << concatenated.size() << " characters" << std::endl;

    // Append
    std::string appended = first;
    appended.append(" there!");
    std::cout << "\tAppend:\t\t\t" << "'" << first << "' appended with ' there!' = " << appended << std::endl;

    // Insert
    std::string inserted = second;
    inserted.insert(0, "Hello ");
    std::cout << "\tInsert:\t\t\t" << "insert 'Hello ' to '" << second << "' at index 0 = " << inserted << std::endl;

    // Erase
    std::string erased = inserted;
    erased.erase(0, 6); // Erase "Hello "
    std::cout << "\tErase:\t\t\t" << "erase 'Hello ' from '" << inserted << "' = " << erased << std::endl;

    // Compare
    std::cout << "\tCompare:\t\t" << "'" << first << "' compared to '" << second << "' = " << first.compare(second) << std::endl;

    // Add more string operations as necessary...
}
