#include <iostream>
#include <string>

void showVariableTypes() {
    std::cout << "The following are the basic variable types in C++" << std::endl;

    // Integer type
    int myInteger = 5; // Whole number
    std::cout << "\tInteger: " << myInteger << std::endl;

    // Floating point type
    float myFloat = 5.99; // Floating point number
    std::cout << "\tFloat: " << myFloat << std::endl;

    // Double floating point type
    double myDouble = 9.98; // Double floating point number
    std::cout << "\tDouble: " << myDouble << std::endl;

    // Character type
    char myLetter = 'D'; // Single character
    std::cout << "\tChar: " << myLetter << std::endl;

    // Boolean type
    bool myBoolean = true; // Boolean value true or false
    std::cout << "\tBoolean: " << myBoolean << std::endl;

    // String type (Using string class from the C++ Standard Library)
    std::string myString = "Hello"; // String of characters
    std::cout << "\tString: " << myString << std::endl;

    // Array type
    int myArray[3] = {10, 20, 30}; // Array of integers
    std::cout << "\tArray: ";
    for (int i = 0; i < 3; ++i) {
        std::cout << myArray[i] << " ";
    }
    std::cout << std::endl;
}
