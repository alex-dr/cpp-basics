// ExceptionHandling.cpp
#include "../include/ExceptionHandling.h"
#include <iostream>
#include <stdexcept> // For standard exception types
#include <string>

void showExceptionHandling() {
    std::cout << "Demonstrating exception handling in C++:" << std::endl;

    // std::out_of_range
    try {
        std::string str = "Hello";
        // Attempt to access beyond the string's range to trigger an exception
        char ch = str.at(10); // This will throw std::out_of_range
    } catch (const std::out_of_range& e) {
        std::cout << "\tCaught an std::out_of_range exception: " << e.what() << std::endl;
    }

    // std::invalid_argument
    try {
        int num = std::stoi("not_a_number"); // This will throw std::invalid_argument
    } catch (const std::invalid_argument& e) {
        std::cout << "\tCaught an std::invalid_argument exception: " << e.what() << std::endl;
    }

    // std::bad_alloc
    try {
        // Attempt to allocate an absurd amount of memory to trigger std::bad_alloc
        int* largeArray = new int[1000000000000];
        delete[] largeArray;
    } catch (const std::bad_alloc& e) {
        std::cout << "\tCaught an std::bad_alloc exception: " << e.what() << std::endl;
    }

    // Catching all exceptions
    try {
        throw 20; // Throw an integer (not a standard exception type)
    } catch (...) { // Catch all handler
        std::cout << "\tCaught an exception of an unknown type" << std::endl;
    }
}
