// ArrayOperations.cpp
#include "../include/ArrayOperations.h"
#include <iostream>

void showArrayOperations() {
    std::cout << "Demonstrating array operations in C++:" << std::endl;

    // Initializing an array
    int myArray[5] = {1, 2, 3, 4, 5};
    std::cout << "\tInitialized array: ";
    for (int i = 0; i < 5; ++i) {
        std::cout << myArray[i] << " ";
    }
    std::cout << std::endl;

    // Accessing elements
    std::cout << "\tAccessing the third element: " << myArray[2] << std::endl;

    // Modifying elements
    myArray[2] = 10;
    std::cout << "\tAfter modifying the third element to 10: ";
    for (int i = 0; i < 5; ++i) {
        std::cout << myArray[i] << " ";
    }
    std::cout << std::endl;

    // Iterating over the array
    std::cout << "\tIterating over the array: ";
    for (int element : myArray) {
        std::cout << element << " ";
    }
    std::cout << std::endl;

    // Multi-dimensional array initialization and iteration
    int twoDimArray[2][3] = {
        {1, 2, 3},
        {4, 5, 6}
    };
    std::cout << "\tTwo-dimensional array elements:" << std::endl;
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cout << "\t\tElement at [" << i << "][" << j << "]: " << twoDimArray[i][j] << std::endl;
        }
    }
}
